<!DOCTYPE html>
<html>
    <head>
        <?php
        //Conexxion mysql a la base de datos
        include 'librerias.php'; //se mandan a llamar los scripts y hojas de estilo para reutilizar codigo
        ?>
    </head>
    <body>
        <!-- Creacion de los marcos y bordes de las secciones-->
        <div id="marco"> <!-- Contiene todas las secciones y las ajusta conforme al css -->
            <contenedor> <!-- contiene  todas las secciones interiores -->
                <header>
                    <?php
                    // se despliega el logo junto con el menu y el buscador esto para reutilizar codigo
                    include 'menu.php';
                    ?>
                </header> 
                <center>
                    <?php
                    //consulta de articulos dentro de la base de datos limitandolo a 8
                    $consulta = mysql_query("select * from Articulos LIMIT 0,8");
                    //while para desplegar todos los articulos hasta el limite establecido en la consulta
                    while ($filas = mysql_fetch_array($consulta)) {
                        $id = $filas['idArticulos'];
                        $ruta = $filas['imagen'];
                        $desc = $filas['descripcion'];
                        $nombre = $filas['nombre'];
                        echo'<div class="caja">';
                        include 'caja.php'; // caja contenedora reutilizacion de codigo
                        echo '</div>';
                    }
                    ?>
                </center> 
                <p></p>
                <pie>
                    Relevantes
                    <br>
                    <?php
                    //consulta de articulos ordenadas aleatoriamente (rand) limitado a 4 
                    $consulta = mysql_query("select * from Articulos ORDER BY RAND() LIMIT 0,4");
                    //while para desplegar todos los articulos hasta el limite establecido en la consulta
                    while ($filas = mysql_fetch_array($consulta)) {
                        $id = $filas['idArticulos'];
                        $ruta = $filas['imagen'];
                        $desc = $filas['descripcion'];
                        $nombre = $filas['nombre'];
                        echo '<div class="caja">';
                        include 'caja.php'; // caja contenedora reutilizacion de codigo
                        echo '</div>';
                    }
                    ?>
                </pie><!-- end pie -->
            </contenedor><!-- end contenedor -->
        </div><!-- end marco -->
    </body><!-- end body-->
</html>