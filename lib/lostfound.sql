SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `a6721025_base` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `a6721025_base` ;

-- -----------------------------------------------------
-- Table `a6721025_base`.`Articulos`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `a6721025_base`.`Articulos` (
  `idArticulos` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NOT NULL ,
  `imagen` VARCHAR(45) NOT NULL ,
  `descripcion` VARCHAR(45) NOT NULL ,
  `activo` VARCHAR(45) NOT NULL ,
  `categoria` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idArticulos`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `a6721025_base`.`Usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `a6721025_base`.`Usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT ,
  `nombre` VARCHAR(45) NOT NULL ,
  `nick` VARCHAR(45) NOT NULL ,
  `pswd` VARCHAR(45) NOT NULL ,
  `imagen` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`idUsuario`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `a6721025_base`.`tags`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `a6721025_base`.`tags` (
  `idtags` INT NOT NULL AUTO_INCREMENT ,
  `etiqueta` VARCHAR(45) NOT NULL ,
  `Articulos_idArticulos` INT NOT NULL ,
  PRIMARY KEY (`idtags`, `Articulos_idArticulos`) ,
  INDEX `fk_tags_Articulos_idx` (`Articulos_idArticulos` ASC) ,
  CONSTRAINT `fk_tags_Articulos`
    FOREIGN KEY (`Articulos_idArticulos` )
    REFERENCES `a6721025_base`.`Articulos` (`idArticulos` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `a6721025_base`.`status`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `a6721025_base`.`status` (
  `idEstatus` INT NOT NULL AUTO_INCREMENT ,
  `fechaA` DATE NOT NULL ,
  `fechaB` DATE NULL ,
  `Articulos_idArticulos` INT NOT NULL ,
  `Usuario_idUsuarioA` INT NOT NULL ,
  `Usuario_idUsuarioB` INT NULL ,
  PRIMARY KEY (`idEstatus`, `Usuario_idUsuarioA`, `Usuario_idUsuarioB`) ,
  INDEX `fk_status_Articulos1_idx` (`Articulos_idArticulos` ASC) ,
  INDEX `fk_status_Usuario1_idx` (`Usuario_idUsuarioA` ASC) ,
  INDEX `fk_status_Usuario2_idx` (`Usuario_idUsuarioB` ASC) ,
  CONSTRAINT `fk_status_Articulos1`
    FOREIGN KEY (`Articulos_idArticulos` )
    REFERENCES `a6721025_base`.`Articulos` (`idArticulos` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_Usuario1`
    FOREIGN KEY (`Usuario_idUsuarioA` )
    REFERENCES `a6721025_base`.`Usuario` (`idUsuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_status_Usuario2`
    FOREIGN KEY (`Usuario_idUsuarioB` )
    REFERENCES `a6721025_base`.`Usuario` (`idUsuario` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `a6721025_base` ;
USE `a6721025_base`;

DELIMITER $$
USE `a6721025_base`$$


CREATE TRIGGER `status_UPDATE` AFTER UPDATE ON status FOR EACH ROW
-- Edit trigger body code below this line. Do not edit lines above this one
DELETE t1,t2,t3 FROM Articulos AS t1, status AS t2, tags AS t3 
WHERE t1.idArticulos = t2.Articulos_idArticulos 
AND t1.idArticulos = t3.Articulos_idArticulos 
AND DATE_ADD(t2.fechaB,INTERVAL 15 DAY) <= NOW()
$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `a6721025_base`.`Usuario`
-- -----------------------------------------------------
START TRANSACTION;
USE `a6721025_base`;
INSERT INTO `a6721025_base`.`Usuario` (`idUsuario`, `nombre`, `nick`, `pswd`) VALUES (1, 'root', 'root', 'admin');

COMMIT;
